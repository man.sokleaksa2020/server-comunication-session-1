// function loop() {
//     return new Promise((resolve, reject) => {
//         let i = 0;
//         while (i < 1000000000) { i++ }
//         // resolve('100M loop done!')
//         reject('Error')
//     })
// }
// console.log('sync 1');
// loop()
//     .then(result => console.log(result))
//     .catch(error => console.log(error))
// console.log('sync 3');

//Async
const myAsync = async () => {
    return 3
}
const getAnimal = (sound) => {
    const animal = {
        moew: "cat",
        whooh: "dog",
    }
    return Promise.resolve(animal[sound])
}
const showAnimal = async () => {
    let a1 = await getAnimal('moew')
    let a2 = await getAnimal('whooh')
    return [a1, a2]
}

// const showAnimal = () => {
//     let re;
//     return getAnimal('moew').then(
//         re1 => {
//             re = re1
//             return getAnimal('whooh')
//         }
//     ).then(
//         re2 => [re, re2]
//     )
// }
showAnimal().then(console.log)