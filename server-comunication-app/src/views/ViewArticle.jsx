import axios from 'axios'
import React,{useState,useEffect} from 'react'
import { Container } from 'react-bootstrap'
import {useParams} from 'react-router'
import { BASE_URL } from '../api/api'
function ViewArticle() {

  const [article, setArticle] = useState()
  const {id} = useParams()

  useEffect(() => {
     axios.get(BASE_URL+'/articles/'+id)
     .then(response=>{
        setArticle(response.data.data)
     })
    }, [])

  return (
    <Container>
        {article ? <>
            <h1>{article.title}</h1>
            <p>{article.description}</p>
        </>:
        ''}
    </Container>
  )
}

export default ViewArticle
