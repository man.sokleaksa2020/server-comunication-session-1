import axios from "axios";
import React,{useEffect,useState} from "react";
import { Button, Container,Table } from "react-bootstrap";
import { useHistory } from "react-router";
import { BASE_URL } from "../api/api";


function Article() {

    const [articles, setArticles] = useState([])

    useEffect( async() => {

        let response = await axios.get(BASE_URL+'/articles?page=1&size=10')
        setArticles(response.data.data)

    }, [])

    let history = useHistory()

    const deleteArticle = (id)=>{
        axios.delete(BASE_URL+`/articles/${id}`).
        then((response)=>{
            let newArticles = articles.filter(article=>article._id !== id )
            setArticles(newArticles)
            alert(response.data.message)
        })
    }

  return (
    <Container>
      <h1 className="text-center my-4">AMS Management</h1>
      <Table striped bordered hover size="sm">
        <thead>
          <tr>
            <th>#</th>
            <th style={{width:"20%" }}>Title</th>
            <th style={{width:"60%" }}>Description</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {
              articles.map((article,index)=>
                    <tr key={article._id}>
                    <td>{index}</td>
                    <td >{article.title}</td>
                    <td >{article.description}</td>
                    <td>
                        <Button size="sm" variant="info"
                            onClick={()=>{
                                history.push('/article/'+article._id)
                            }}
                        >View</Button>
                        <Button size="sm" className="mx-2" variant="warning">Edit</Button>
                        <Button size="sm" variant="danger"
                            onClick={()=>
                                deleteArticle(article._id)
                            }
                        >Delete</Button>
                    </td>
                </tr>
              )
          }
          
          
        </tbody>
      </Table>
    </Container>
  );
}

export default Article;
